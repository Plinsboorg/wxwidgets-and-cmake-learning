cmake_minimum_required(VERSION 3.2.0)
project(wxwidgets_cmake VERSION 0.1.0)

set(wxWidgets_ROOT_DIR "C:/wxwidgets")
set(wxWidgets_LIB_DIR "C:/wxwidgets/lib/gcc1210_x64_dll")

find_package(wxWidgets REQUIRED COMPONENTS net core base)
include(${wxWidgets_USE_FILE})

file(GLOB project_GLOB
    src/*
)

add_executable(wxwidgets_cmake ${project_GLOB})

target_link_libraries(wxwidgets_cmake ${wxWidgets_LIBRARIES})
target_include_directories(wxwidgets_cmake PRIVATE ${wxWidgets_INCLUDE_DIRS})
