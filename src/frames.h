#include <wx/wx.h>
#include "panels.h"

class MyLearningFrame : public wxFrame
{
public:
    MyLearningFrame(const wxString& title);
    void OnQuit(wxCommandEvent & event);

    LeftPanel *m_lp;
    RightPanel *m_rp;
    wxPanel *m_parent;

};