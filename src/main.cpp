// wxWidgets tutorial 
// https://zetcode.com/gui/wxwidgets/firstprograms/ 
// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wx.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include "frames.h"
//#include <wx/stattext.h>
//#include <wx/panel.h>

class MyApp : public wxApp
{
  public:
    virtual bool OnInit()
    {
    wxInitAllImageHandlers();
    MyLearningFrame *myframe = new MyLearningFrame(wxT("Name of my app"));
    myframe->Show(true);

    return true;
    }
};

IMPLEMENT_APP(MyApp)

