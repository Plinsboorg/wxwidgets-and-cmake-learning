    #include "frames.h"

    MyLearningFrame::MyLearningFrame(const wxString& title): 
            wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(350, 350))
    {
    //adding panels to the frame
    wxPanel *parent_panel = new wxPanel(this, wxID_ANY);
    wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);
    m_lp = new LeftPanel(parent_panel);
    m_rp = new RightPanel(parent_panel);
    hbox->Add(m_lp, 1, wxEXPAND | wxALL, 5);
    hbox->Add(m_rp, 1, wxEXPAND | wxALL, 5);
    parent_panel->SetSizer(hbox);

    //add icon
    wxIcon FrameIcon;
    FrameIcon.CopyFromBitmap(wxBitmap(wxImage(_T("C:\\Users\\denis\\Documents\\GitLab\\wxwidgets-and-cmake-learning\\Resources\\pcbcalculator_small.png"))));
    SetIcon(FrameIcon);

    //add button to the frame
    wxButton *button = new wxButton(parent_panel, wxID_EXIT, wxT("Quit"), 
    wxPoint(40, 40));
    Connect(wxID_EXIT, wxEVT_COMMAND_BUTTON_CLICKED, 
    wxCommandEventHandler(MyLearningFrame::OnQuit));
    button->SetFocus();

    this->Centre();
    }    

void MyLearningFrame::OnQuit(wxCommandEvent & WXUNUSED(event))
{
    Close(true);
}